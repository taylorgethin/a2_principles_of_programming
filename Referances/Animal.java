package com.company;




class Animals
{
    Animals(){
        System.out.println("Animal Created");
    }

    String food;
    String location;
    int hunger;

    // add more things an animal has and a constructor
    void makeNoise()
    {
     // do all animals make noise
        System.out.println("do all animals make noise");
    }
    void eat()
    {
        //each animal can eat different things
        System.out.println("each animal can eat different things");
    }
    void sleep()
    {
        // all animals sleep
        System.out.println("all animals sleep");
    }
    void roam()
    {
        // all animals roam#
        System.out.println("Animals move around");
    }
}

class Canine extends Animals
{
    Canine(){
        System.out.println("Canine Created");
    }

    void roam()
    {
        //this makes a type of roaming unique to canines
        System.out.println("Canines roam a large area");
    }
    void roam(Feline a)
    {
        //this makes a type of roaming unique to canines
        System.out.println("Canines...");
    }

}

class dogs extends Canine
{
    String breed;
     dogs()
     {
        breed = "unknown";
        location = "home";
        hunger = 100;
        food = "meat";
     }
    dogs(String breed_type)
    {
        breed = breed_type;
    }
    void type(){
         System.out.println(breed);
    }
}

class wolf extends Canine
{
    String breed;
    wolf()
    {
        breed = "unknown";
        location = "Woods";
        hunger = 100;
        food = "meat";
    }

    wolf(String breed_type)
    {
        breed = breed_type;
    }
    void type(){
        System.out.println(breed);
    }
}


class Feline extends Animals
{
    void roam()
    {
        //this makes a type of roaming unique to canines
        System.out.println("Felines tend to stick to their own patch.");
    }

}

class Overload{
    String uniqueID = "";
    public void setUniqueID(String s)
    {
        uniqueID=s;
    }

    public void setUniqueID(Integer b)
    {
        String numberString = "" + b;
        setUniqueID(numberString);
    }

    public int addNumbers(int a, int b){
        return a+b;
    }
    public double addNumbers(double a, double b) {
        return a + b;
    }
    public static void main(String[] args) {
        Overload x = new Overload();
        System.out.println(x.addNumbers(1.5,2.4));
        x.setUniqueID(".");
    }
}

class TestClass{
    public static void main(String[] args) {
        Overload x = new Overload();
        x.setUniqueID(223);
        System.out.println(x.uniqueID);
    }
}


public class Main {


    public static void main(String[] args) {
	// write your code here

        Animals c = new Animals();
        Canine d = new Canine();
        Feline f = new Feline();
        wolf w = new wolf();
        dogs doge = new dogs();


        d.roam(f);




        //dogs q = new dogs("bull");

//        d.roam();
        //q.eat();
        //q.type();


//        c.sleep();
//        c.roam();
//        c.eat();
//        c.makeNoise();

    }
}
