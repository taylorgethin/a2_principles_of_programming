/******************************************************************************
 * \PROGRAM: Assinment 2 of Principles of Computer Programming
 * \BRIEF: This system will allow the encryption and decryption of messages
 * based on settings defined in a text file.
 ******************************************************************************/

/******************************************************************************
 * INCLUDE FILES
 ******************************************************************************/
package com.company;

import javafx.animation.PauseTransition;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/*-----------------------------------------------------------------------------
 * CLASSESS
 *-----------------------------------------------------------------------------*/

// Read and set class. this class contains the functions to read the text file
// and write back to it in the correct format
class ReadSetFile {

    // enum collective user info storage
    enum user_info_t {
        message,
        username,
        forename,
        surname,
        age,
        time,
        algorithm,
    }

    //initialising the data store variables
    String result = "";
    String line_username = "";
    String line_surname = "";
    String line_age = "";
    String line_time = "";

    //Private variables
    private String line_forename = "";
    private String line_algorithm = "";
    private String line_message = "";

    // Set function for time to be used by the test class
    public void Set_Line_Time(String time_input) {
        line_time = (time_input != null) ?  time_input : "Unknown";
    }
    // Get function for time to be used by the test class
    public String Get_Line_Time() {
        return line_time;
    }
    // Set function for age to be used by the test class
    public void Set_Line_Age(String age_input) {
        line_age = (age_input != null) ? age_input : "Unknown";
    }
    // Get function for age to be used by the test class
    public String Get_Line_Age() {
        return line_age;
    }
    // Set function for forename to be used outside the private characteristics
    public void Set_Line_Forename(String forname_input) {
        line_forename = (forname_input != null) ? forname_input : "Unknown";
    }
    // Get function for forename to be used outside the private characteristics
    public String Get_Line_Forename() {
        return line_forename;
    }
    // Set function for surname to be used by the test class
    public void Set_Line_Surname(String surname_input) {
        line_surname = (surname_input != null) ? surname_input : "Unknown";
    }
    // Get function for surname to be used by the test class
    public String Get_Line_Surname() {
        return line_surname;
    }
    // Set function for username to be used by the test class
    public void Set_Line_Username(String username_input) {
        line_username = (username_input != null) ? username_input : "Unknown";
    }
    // Get function for username to be used by the test class
    public String Get_Line_Username() {
        return line_username;
    }
    // Set function for algorithm to be used outside the private characteristics
    public void Set_Line_Algorithm(String algorithm_input) {
        line_algorithm = (algorithm_input != null) ? algorithm_input : "Unknown";
    }
    // Get function for algorithm to be used outside the private characteristics
    public String Get_Line_Algorithm() {
        return line_algorithm;
    }
    // Set function for message to be used outside the private characteristics
    public void Set_Line_Message(String message_input) {
        line_message = (message_input != null) ? message_input : "Unknown";
    }
    // Get function for message to be used outside the private characteristics
    public String Get_Line_Message() {
        return line_message;
    }

    /*
    * Read function. File reads each line and stores the data into an array. It then reads each line looking for
    * keywords and storing the information needed after a substring format. these are then stored into the enum
    * relevant variables. these variables are then put into a switch statement returning for the requested data.
    */
    String Read_UI_To_File(user_info_t userinfo) {
        try {
            String line = " ";
            ArrayList<String> line_data = new ArrayList<String>();
            BufferedReader buff_read = new BufferedReader(new FileReader("src/Absolutely not the encryption file/SuperSecretMessage.txt"));
            while ((line = buff_read.readLine()) != null) {
                line_data.add(line);
            }
            for (int i = 0; line_data.size() - 1 >= i; i++) {
                String words = line_data.get(i);
                if (words != null) {
                    if (words.contains("Username")) {
                        line_username = words.substring(words.indexOf("Username =") + 11);
                    } else if (words.contains("Forename")) {
                        Set_Line_Forename(words.substring(words.indexOf("Forename =") + 11));
                    } else if (words.contains("Surname")) {
                        line_surname = words.substring(words.indexOf("Surname =") + 10);
                    } else if (words.contains("Age")) {
                        line_age = words.substring(words.indexOf("Surname =") + 7);
                    } else if (words.contains("Time")) {
                        line_time = words.substring(words.indexOf("Time =") + 7);
                    } else if (words.contains("Algorithm")) {
                        Set_Line_Algorithm(words.substring(words.indexOf("Algorithm =") + 12));
                    } else if (words.contains("Message")) {
                        Set_Line_Message(words.substring(words.indexOf("Message =") + 10));
                    } else {
                        line_username = "Unknown";
                        line_age = "Unknown";
                        Set_Line_Forename("Unknown");
                        Set_Line_Message("Unknown");
                        Set_Line_Algorithm("Unknown");
                        line_surname = "Unknown";
                        line_time = "Unknown";
                        break;
                    }
                }

                    buff_read.close();
                }
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (userinfo) {
            case username: {
                result = line_username;
                break;
            }
            case message: {
                result = Get_Line_Message();
                break;
            }
            case forename: {
                result = Get_Line_Forename();
                break;
            }
            case surname: {
                result = line_surname;
                break;
            }
            case age: {
                result = line_age;
                break;
            }
            case time: {
                result = line_time;
                break;
            }
            case algorithm: {
                result = Get_Line_Algorithm();
                break;
            }
            default: {
                result = "the user information you have entered doesnt exist";
                break;
            }
        }
        return result;
    }

    /*
     * Set function. with one parameter clears the test file.
     */
    public void Set_UI_To_File(String clear) {
        try {
            BufferedWriter clear_file = new BufferedWriter(new FileWriter("src/Absolutely not the encryption file/SuperSecretMessage.txt"));
            clear_file.write(" ");
        } catch (Exception e) {
            System.out.println("Cannot clear file. please check file exists");
        }
    }

    /*
     * Set function. Has 2 parameters. prog_context is used for the switch statement. this switches between each espected
     * setting, prog_context is also turned to uppercase removing user error. prog_data is the data that is stored
     * for each possible setting.
     */
    void Set_UI_To_File(String prog_context, String prog_data) {
        try {
            BufferedWriter buff_write = new BufferedWriter(new FileWriter("src/Absolutely not the encryption file/SuperSecretMessage.txt", true));
            if (prog_data != null) {
                String context = prog_context.toUpperCase();
                switch (context) {
                    case "USERNAME":
                        buff_write.write("Username = " + prog_data);
                        buff_write.newLine();
                        buff_write.close();
                        break;
                    case "FORENAME":
                        buff_write.write("Forename = " + prog_data);
                        buff_write.newLine();
                        buff_write.close();
                        break;
                    case "SURNAME":
                        buff_write.write("Surname = " + prog_data);
                        buff_write.newLine();
                        buff_write.close();
                        break;
                    case "AGE":
                        buff_write.write("Age = " + prog_data);
                        buff_write.newLine();
                        buff_write.close();
                        break;
                    case "ALGORITHM":
                        buff_write.write("Algorithm = " + prog_data);
                        buff_write.newLine();
                        buff_write.close();
                        break;
                    case "TIME":
                        buff_write.write("Time = " + prog_data);
                        buff_write.newLine();
                        buff_write.close();
                        break;
                    case "MESSAGE":
                        buff_write.write("Message = " + prog_data);
                        buff_write.newLine();
                        buff_write.close();
                        break;
                    default:
                        buff_write.close();

                }
            }
        } catch (Exception e) {
            System.out.println("Error occurred when setting data");
        }
    }
}

/*
 * Choose encrypt class. the encryption function takes your encryption type, the message you want to encrypt and
 * a cypher key. firstly the function runs a timer if it is not currently testing. at the end of the countdown
 * the encryption begins. the program takes the encryption_type parameter and converts it into uppercase which is
 * searched for in a switch statement, each case is the possible user imputed data. each encryption is
 * explained in my report within the relevant section. the encrypted message is stored in result and returned
 * at the end of the encryption.
 */
class Choose_Encrypt {
    boolean testing_encrypt = true;
    String result = " ";

    String Choose_which_Encrypt(String encryption_type, String user_message, String cypher_key) {
        String encryption_upper = encryption_type.toUpperCase();
        if (!testing_encrypt) {
            System.out.println("Encrypting");
            try {
                System.out.println(1);
                java.util.concurrent.TimeUnit.SECONDS.sleep(1);
                System.out.println(2);
                java.util.concurrent.TimeUnit.SECONDS.sleep(1);
                System.out.println(3);
                java.util.concurrent.TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
            switch (encryption_upper) {
                case "ROT13":
                    StringBuilder rot13 = new StringBuilder();
                    for (int i = 0; i < user_message.length(); i++) {
                        char CharUI = user_message.charAt(i);
                        if (CharUI >= 'a' && CharUI <= 'm') CharUI += 13;
                        else if (CharUI >= 'A' && CharUI <= 'M') CharUI += 13;
                        else if (CharUI >= 'n' && CharUI <= 'z') CharUI -= 13;
                        else if (CharUI >= 'N' && CharUI <= 'Z') CharUI -= 13;
                        rot13.append(CharUI);
                    }
                    result = rot13.toString();
                    break;
                case "CIPHER":
                    StringBuilder encryptedMessage = new StringBuilder();
                    int int_cypher_key = Integer.parseInt(cypher_key);
                    for(int i = int_cypher_key; i > 26; i-- )
                    {
                        int_cypher_key = i;
                    }
                    int key = int_cypher_key; // cannot be higher than 26 as this will give invalid characters
                    char ch;
                    Scanner sc = new Scanner(System.in);

                    for (int i = 0; i < user_message.length(); ++i) {
                        ch = user_message.charAt(i);

                        if (ch >= 'a' && ch <= 'z') {
                            ch = (char) (ch + key);

                            if (ch > 'z') {
                                ch = (char) (ch - 'z' + 'a' - 1);
                            }

                            result = (encryptedMessage.append(ch).toString());
                        } else if (ch >= 'A' && ch <= 'Z') {
                            ch = (char) (ch + key);

                            if (ch > 'Z') {
                                ch = (char) (ch - 'Z' + 'A' - 1);
                            }

                            result = (encryptedMessage.append(ch).toString());
                        } else {
                            result = (encryptedMessage.append(ch).toString());
                        }
                    }
                    break;
                case "REVERSE":
                    /*
                     * This Function is used for reversing a string. Firstly I create a variable
                     * that holds the result of the reverse function in a buffer. the result is
                     * converted to a string so it can be used to display.
                     */
                    result = new StringBuffer(user_message).reverse().toString();
                    break;
                default:
                    System.out.println("No information here");
                    break;
            }
        if (!testing_encrypt) {
            System.out.println("Encryption Complete");
            System.out.println("\nEncryption Message = " + result);
        }
        return result;
    }
}

/*
 * Choose_Decrypt class. the decryption function takes your decryption type, the message you want to decrypt and
 * a cypher key. The decryption function runs much like the encryption other than it is doing the opposite in
 * regards to encryption. rot13 and reverse functions simply need to be reapplied to decrypt them. But cypher
 * needs to invert the calculation done in the encryption.
 */
class Choose_Decrypt {
    String result = " ";

    String Choose_which_Decrypt(String decryption_type, String user_message, String cypher_key) {
        String decrypt_password = "mypassword";
        System.out.println("\nEnter Password For Decryption");
        Scanner scanIn = new Scanner(System.in);
        String password = scanIn.nextLine();
        if (password.equals(decrypt_password)) {
            String encryption_upper = decryption_type.toUpperCase();
            System.out.println("\nPassword accepted\n");
            switch (encryption_upper) {
                case "ROT13":
                    StringBuilder rot13 = new StringBuilder();
                    for (int i = 0; i < user_message.length(); i++) {
                        char CharUI = user_message.charAt(i);
                        if (CharUI >= 'a' && CharUI <= 'm') CharUI += 13;
                        else if (CharUI >= 'A' && CharUI <= 'M') CharUI += 13;
                        else if (CharUI >= 'n' && CharUI <= 'z') CharUI -= 13;
                        else if (CharUI >= 'N' && CharUI <= 'Z') CharUI -= 13;
                        rot13.append(CharUI);
                    }
                    result = rot13.toString();
                    break;
                case "CIPHER":
                    String result_decrypt = "";
                    int int_cypher_key = Integer.parseInt(cypher_key);
                    for(int i = int_cypher_key; i > 26; i-- )
                    {
                        int_cypher_key = i;
                    }
                    int key = int_cypher_key;
                    char ch;
                    for(int i = 0; i < user_message.length(); ++i){
                        ch = user_message.charAt(i);

                        if(ch >= 'a' && ch <= 'z'){
                            ch = (char)(ch - key);

                            if(ch < 'a'){
                                ch = (char)(ch + 'z' - 'a' + 1);
                            }

                            result_decrypt += ch;
                        }
                        else if(ch >= 'A' && ch <= 'Z'){
                            ch = (char)(ch - key);

                            if(ch < 'A'){
                                ch = (char)(ch + 'Z' - 'A' + 1);
                            }

                            result_decrypt += ch;
                        }
                        else {
                            result_decrypt += ch;
                        }
                        result = result_decrypt;
                    }
                    break;
                case "REVERSE":
                    /*
                     * This Function is used for reversing a string. Firstly I create a variable
                     * that holds the result of the reverse function in a buffer. the result is
                     * converted to a string so it can be used to display.
                     */
                    result = new StringBuffer(user_message).reverse().toString();
                    break;
                default:
                    System.out.println("\nNo information here\n");
                    break;
            }
        }
        else
        {
            System.out.println("\nPassword not accepted\n");
            result = "incorrect password given";
        }
        return result;
    }
}

/*
 * Restart_File class. the reset file is simply used to reset the file back to its previous condition.
 * This class is used after reading, encrypting and decrypting. The class resets the file to its previous
 * condition after requesting the user of the program whether they want to reset the file. The function will request
 * a yes and no answer to this yes which resets the file and no which doesn’t affect it. The function uses the
 *  read and sets function within the program to clear the file and set the data.
 */
class Restart_File{
    void reset_file() {

        System.out.println("\nDo you want to reset the file?");
        Scanner scanIn = new Scanner(System.in);
        String answer = scanIn.nextLine();
        String answer_result = answer.toUpperCase();
        if(answer_result.equals("YES")) {
            ReadSetFile read = new ReadSetFile();
            String surname_backup = read.Read_UI_To_File(ReadSetFile.user_info_t.surname);
            String forename_backup = read.Get_Line_Forename();
            String username_backup = read.Get_Line_Username();
            String age_backup = read.Get_Line_Age();
            String time_backup = read.Get_Line_Time();
            String algorithm_backup = read.Get_Line_Algorithm();
            String message_backup = read.Get_Line_Message();


            ReadSetFile set = new ReadSetFile();
            set.Set_UI_To_File("clear");
            set.Set_UI_To_File("username",username_backup);
            set.Set_UI_To_File("forename",forename_backup);
            set.Set_UI_To_File("surname",surname_backup);
            set.Set_UI_To_File("age",age_backup);
            set.Set_UI_To_File("time",time_backup);
            set.Set_UI_To_File("algorithm",algorithm_backup);
            set.Set_UI_To_File("message",message_backup);//"You will have to put a message here");
            System.out.println("File is reset to previous condition");
        }
        else if (answer_result.equals("NO"))
        {
            System.out.println("You selected no the file will have to be reset manually");
        }
    }
}

/*
 * Testing class. This test class runs an automatic interface and functional test of all my functions with expected
 * and unexpected inputs, it could be more elaborate, but I feared the test would get too large. The test begins by
 * taking a copy of the text file for restoring after the test. The function then runs through all setters and
 * getters ensuring correct response to some inputs. The test then goes into a functionality-based test.
 * It encrypts and decrypts messages with expected responses. This test will return pass or fail for each test.
 * If the response is incorrect the test will output an expected response. It then restores the previously taken
 * variables back to the file.
 */
class Testing{
    String Testing_File()
    {
        String result = "";
        result += "\nBackup stored successfully\n";
        String surname_backup = ReadSetFile.user_info_t.surname.toString();
        String forename_backup = ReadSetFile.user_info_t.forename.toString();
        String username_backup = ReadSetFile.user_info_t.username.toString();
        String age_backup = ReadSetFile.user_info_t.age.toString();
        String time_backup = ReadSetFile.user_info_t.time.toString();
        String algorithm_backup = ReadSetFile.user_info_t.algorithm.toString();
        String message_backup = ReadSetFile.user_info_t.message.toString();
        //test the flow throwing dummy values
        ReadSetFile Read_Set = new ReadSetFile();

        result += "\nBegin Test\n\n";
        result += !surname_backup.equals("Unknown") ? "passed surname read\n" : "\nFailed surname test\n";
        result += !username_backup.equals("Unknown") ? "passed username read\n" : "\nFailed username test\n";
        result += !forename_backup.equals("Unknown") ? "passed forename read\n" : "\nFailed forename test\n";
        result += !age_backup.equals("Unknown") ? "passed age read\n" : "\nFailed age test\n";
        result += !time_backup.equals("Unknown") ? "passed time read\n" : "\nFailed time test\n";
        result += !algorithm_backup.equals("Unknown") ? "passed algorithm read\n" : "\nFailed algorithm test\n";
        result += !message_backup.equals("Unknown") ? "passed message read\n" : "\nFailed message test\n";

        Read_Set.Set_Line_Forename(null);
        result += (Read_Set.Get_Line_Forename().equals("Unknown") ? "Passed forename set to null test\n" :
                "\nFail forename set to null test\n");
        Read_Set.Set_Line_Algorithm(null);
        result += (Read_Set.Get_Line_Algorithm().equals("Unknown") ? "Passed algorithm set to null test\n" :
                "\nFail algorithm set to null test\n");
        Read_Set.Set_Line_Message(null);
        result += (Read_Set.Get_Line_Message().equals("Unknown") ? "Passed message set to null test\n" :
                "\nFail message set to null test\n");
        Read_Set.Set_Line_Username(null);
        result += (Read_Set.Get_Line_Username().equals("Unknown") ? "Passed username set to null test\n" :
                "\nFail username set to null test\n");
        Read_Set.Set_Line_Surname(null);
        result += (Read_Set.Get_Line_Surname().equals("Unknown") ? "Passed username set to null test\n" :
                "\nFail username set to null test\n");
        Read_Set.Set_Line_Age(null);
        result += (Read_Set.Get_Line_Age().equals("Unknown") ? "Passed age set to null test\n" :
                "\nFail age set to null test\n");
        Read_Set.Set_Line_Time(null);
        result += (Read_Set.Get_Line_Time().equals("Unknown") ? "Passed time set to null test\n" :
                "\nFail time set to null test\n");

        //Expected fails and passes
        Read_Set.Set_Line_Forename("abc");
        result += (Read_Set.Get_Line_Forename().equals("cba") ? "\nPassed forename Unexpectedly\n" :
                "Fail forename as expected\n");
        result += (Read_Set.Get_Line_Forename().equals("abc") ? "Passed forename set to value\n" :
                "\nFail forename set to value\n");

        Read_Set.Set_Line_Algorithm("abc");
        result += (Read_Set.Get_Line_Algorithm().equals("cba") ? "\nPassed algorithm Unexpectedly\n" :
                "Fail algorithm as expected\n");
        result += (Read_Set.Get_Line_Algorithm().equals("abc") ? "Passed algorithm set to value\n" :
                "\nFail algorithm set to value\n");

        Read_Set.Set_Line_Message("abc");
        result += (Read_Set.Get_Line_Message().equals("cba") ? "\nPassed message unexpectedly\n" :
                "Fail message as expected\n");
        result += (Read_Set.Get_Line_Message().equals("abc") ? "Passed message set to value\n" :
                "\nFail message set to value\n");

        Read_Set.Set_Line_Username("abc");
        result += (Read_Set.Get_Line_Username().equals("cba") ? "\nPassed username unexpectedly\n" :
                "Fail username as expected\n");
        result += (Read_Set.Get_Line_Username().equals("abc") ? "Passed username set to value\n" :
                "\nFail username set to value\n");

        Read_Set.Set_Line_Surname("abc");
        result += (Read_Set.Get_Line_Surname().equals("cba") ? "\nPassed surname unexpectedly\n" :
                "Fail surname as expected\n");
        result += (Read_Set.Get_Line_Surname().equals("abc") ? "Passed surname set to value\n" :
                "\nFail surname as expected\n");

        Read_Set.Set_Line_Age("abc");
        result += (Read_Set.Get_Line_Age().equals("cba") ? "\nPassed age unexpectedly\n" :
                "Fail age as expected\n");
        result += (Read_Set.Get_Line_Age().equals("abc") ? "Passed age set to value\n" :
                "\nFail age set to value\n");

        Read_Set.Set_Line_Time("abc");
        result += (Read_Set.Get_Line_Time().equals("cba") ? "\nPassed time unexpectedly\n" :
                "Fail time as expected\n");
        result += (Read_Set.Get_Line_Time().equals("abc") ? "Passed time set to value\n" :
                "\nFail time set to value\n");

        //Encryption test
        Choose_Encrypt encrypt = new Choose_Encrypt();
        String rot13_encryption_words = encrypt.Choose_which_Encrypt("rot13", "message", "20");

        if(rot13_encryption_words.equals("zrffntr"))
        {
            result += "passed rot 13 encryption test 1";
        }
        else if(rot13_encryption_words == "message")
        {
            result += "\nFail encryption not functioned correctly\n";
        }
        else
        {
            result += "\n\nfailed test expected: " + rot13_encryption_words + "\n";
        }

        //numbers don't rot13 therefore this test is to ensure no change happens
        String rot13_encryption_numbers = encrypt.Choose_which_Encrypt("rot13", "123", "20");

        if(rot13_encryption_numbers.equals("123"))
        {
            result += "\npassed rot 13 encryption test 2";
        }
        else
        {
            result += "\n\nfailed test expected: " + rot13_encryption_numbers + "\n";
        }

        String cipher_encryption_words = encrypt.Choose_which_Encrypt("cipher", "message","20");
        if(cipher_encryption_words.equals("gymmuay"))
        {
            result += "\npassed cipher encryption test 1";
        }
        else if(cipher_encryption_words == "message")
        {
            result += "\nFail encryption not functioned correctly\n";
        }
        else
        {
            result += "\n\nfailed test expected: " + cipher_encryption_words + "\n";
        }
        //should not change the cipher. tried stopping string numbers using parse int calculation and
        // NumberUtils.isParsable
        String cipher_encryption_numbers = encrypt.Choose_which_Encrypt("cipher", "123", "20");
        if(cipher_encryption_numbers.equals("123"))
        {
            result += "\npassed cipher encryption test 2";
        }
        else
        {
            result += "\n\nfailed test expected: " + cipher_encryption_numbers + "\n";
        }

        String reverse_encryption_words = encrypt.Choose_which_Encrypt("reverse", "message", "20");
        if(reverse_encryption_words.equals("egassem"))
        {
            result += "\npassed reverse encryption test 1";
        }
        else if(reverse_encryption_words.equals("message"))
        {
            result += "\nFail encryption not functioned correctly\n";
        }
        else
        {
            result += "\n\nfailed test expected: " + reverse_encryption_words + "\n";
        }

        String reverse_encryption_numbers = encrypt.Choose_which_Encrypt("reverse", "123", "20");
        if(reverse_encryption_numbers.equals("321"))
        {
            result += "\npassed reverse encryption test 2";
        }
        else if(reverse_encryption_numbers.equals("123"))
        {
            result += "\nFail encryption not functioned correctly\n";
        }
        else
        {
            result += "\n\nfailed test expected: " + reverse_encryption_numbers + "\n";
        }

        //Encryption test
        Choose_Decrypt decrypt = new Choose_Decrypt();

        String rot13_decryption_words = decrypt.Choose_which_Decrypt("rot13", "words", "20");
        if(rot13_decryption_words.equals("jbeqf"))
        {
            result += "\npassed rot13 decryption test 1";
        }
        else if(rot13_decryption_words.equals("words"))
        {
            result += "\nFail decryption not functioned correctly\n";
        }
        else
        {
            result += "\n\nfailed test expected: " + rot13_decryption_words + "\n";
        }

        String rot13_decryption_numbers = decrypt.Choose_which_Decrypt("rot13", "123","20");
        if(rot13_decryption_numbers.equals("123"))
        {
            result += "\npassed rot13 decryption test 2";
        }
        else
        {
            result += "\n\nfailed test expected: " + rot13_decryption_numbers + "\n";
        }

        String cipher_decryption_words = decrypt.Choose_which_Decrypt("cipher", "words","20");
        if(cipher_decryption_words.equals("cuxjy"))
        {
            result += "\npassed cipher decryption test 1";
        }
        else if(cipher_decryption_words.equals("words"))
        {
            result += "\nFail decryption not functioned correctly\n";
        }
        else
        {
            result += "\n\nfailed test expected: " + cipher_decryption_words + "\n";
        }

        String cipher_decryption_numbers = decrypt.Choose_which_Decrypt("cipher", "123", "20");
        if(cipher_decryption_numbers.equals("123"))
        {
            result += "\npassed cipher decryption test 2";
        }
        else
        {
            result += "\n\nfailed test expected: " + cipher_decryption_numbers + "\n";
        }

        String reverse_decryption_words = decrypt.Choose_which_Decrypt("reverse", "words", "20");
        if(reverse_decryption_words.equals("sdrow"))
        {
            result += "\npassed reverse decryption test 1";
        }
        else if(reverse_decryption_words.equals("words"))
        {
            result += "\nFail decryption not functioned correctly\n";
        }
        else
        {
            result += "\n\nfailed test expected: " + reverse_decryption_words + "\n";
        }

        String reverse_decryption_numbers = decrypt.Choose_which_Decrypt("reverse", "123", "20");
        if(reverse_decryption_numbers.equals("321"))
        {
            result += "\npassed reverse decryption test 2";
        }
        else if(reverse_decryption_numbers.equals("123"))
        {
            result += "\nFail decryption not functioned correctly\n";
        }
        else
        {
            result += "\n\nfailed test expected: " + reverse_decryption_numbers + "\n";
        }

        result += "\nEnd of Test\n";
        //restoring file to pre test figures
        Restart_File reset = new Restart_File();
        reset.reset_file();
        result += "\nBackup restored successfully\n";
        return result;
    }
}

/******************************************************************************
 * IMPLEMENTATION OF LOCAL (static) FUNCTIONS
 ******************************************************************************/


/******************************************************************************
 * IMPLEMENTATION OF GLOBAL FUNCTIONS / MAIN
 ******************************************************************************/
public class Main {
    public static void main(String[] args)
    {
//        //Defining encrypt object
        Choose_Encrypt encrypt = new Choose_Encrypt();
        Choose_Decrypt decrypt = new Choose_Decrypt();
//        //TESTING CLASS - comment out the program code below.
//        // if you want to test uncomment the test code below
//        encrypt.testing_encrypt = true;
//        Testing test = new Testing();
//        String w = test.Testing_File();
//        System.out.println(w);

        // UNCOMMENT WHEN NOT TESTING
        encrypt.testing_encrypt = false;

        // Defining read set object
        ReadSetFile FileContents = new ReadSetFile();

        //Creating variables that call the user data within the read class
        String username_var = FileContents.Read_UI_To_File(ReadSetFile.user_info_t.username);
        String surname_var = FileContents.Read_UI_To_File(ReadSetFile.user_info_t.surname);
        String age_var = FileContents.Read_UI_To_File(ReadSetFile.user_info_t.age);
        String time_var = FileContents.Read_UI_To_File(ReadSetFile.user_info_t.time);
        String algorithm_var = FileContents.Read_UI_To_File(ReadSetFile.user_info_t.algorithm);
        String message_var = FileContents.Read_UI_To_File(ReadSetFile.user_info_t.message);
        String forename_var = FileContents.Read_UI_To_File(ReadSetFile.user_info_t.forename);

        //Clear file
        FileContents.Set_UI_To_File("clear");

        // Setting previous variables
        FileContents.Set_UI_To_File("username", username_var);
        FileContents.Set_UI_To_File("forename", forename_var);
        FileContents.Set_UI_To_File("surname", surname_var);
        FileContents.Set_UI_To_File("age", age_var);
        FileContents.Set_UI_To_File("time", time_var);
        FileContents.Set_UI_To_File("algorithm", algorithm_var);

        // Encrypt the message with algorithm selected
        String message_encrypt = encrypt.Choose_which_Encrypt(algorithm_var , message_var, age_var);
        FileContents.Set_UI_To_File("message", message_encrypt);
        System.out.println("Encrypted message currently in text file");

        // Decrypt the message with algorithm selected and correct password
        String message_decrypt = decrypt.Choose_which_Decrypt(algorithm_var, message_encrypt, age_var);
        if(message_decrypt.equals("incorrect password given")) {
            System.out.println("Couldn't decrypted the message!");
        }
        else //correct password imputed
        {
            System.out.println("Decrypted the message! Currently in text file");
            FileContents.Set_UI_To_File("message", message_decrypt);
        }

        //resetting the file back to its previous form
        Restart_File reset = new Restart_File();
        reset.reset_file();
    }
}

/******************************************************************************
 * HOST UNIT TESTS
 ******************************************************************************/


/******************************************************************************
 * TARGET UNIT TESTS
 ******************************************************************************/


/******************************************************************************
 * END
 ******************************************************************************/
